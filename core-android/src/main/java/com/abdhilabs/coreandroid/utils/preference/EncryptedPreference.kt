package com.abdhilabs.coreandroid.utils.preference

import android.content.SharedPreferences
import javax.inject.Inject

class EncryptedPreference @Inject constructor(private val sp: SharedPreferences) {

    private val spe: SharedPreferences.Editor by lazy {
        sp.edit()
    }

    fun clear() {
        sp.edit().clear().apply()
    }

    var secureToken: String?
        get() = sp.getString(SP_TOKEN, "")
        set(value) = spe.putString(SP_TOKEN, value).apply()

    var tokenFcm: String?
        get() = sp.getString(SP_TOKEN_FCM, "")
        set(value) = spe.putString(SP_TOKEN_FCM, value).apply()

    var email: String?
        get() = sp.getString(SP_EMAIL, "")
        set(value) = spe.putString(SP_EMAIL, value).apply()

    var name: String?
        get() = sp.getString(SP_NAME, "")
        set(value) = spe.putString(SP_NAME, value).apply()

    var phone: String?
        get() = sp.getString(SP_PHONE, "")
        set(value) = spe.putString(SP_PHONE, value).apply()

    companion object {
        const val SECURE_PREF_NAME = "id.co.hepta.secure_preference"
        const val SP_TOKEN = "pref_token"
        const val SP_EMAIL = "pref_email"
        const val SP_NAME = "pref_name"
        const val SP_PHONE = "pref_phone"
        const val SP_TOKEN_FCM = "pref_token_fcm"
    }
}