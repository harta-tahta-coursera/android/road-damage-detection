package com.abdhilabs.coreandroid.utils.formatter

import java.text.SimpleDateFormat
import java.util.*

object DateTimeFormatter {

    private val dateOutput = SimpleDateFormat("EEEE, dd MMMM yyyy", Locale("in", "ID"))
    private val dateOutputWithoutFormatting = SimpleDateFormat("yyyy-MM-dd", Locale("in", "ID"))
    val timeOutput = SimpleDateFormat("HH:mm:ss", Locale("in", "ID"))

    fun getCurrent(): String {
        val date = Date()
        return dateOutput.format(date)
    }

    fun getCurrentWithoutFormatting(): String {
        val date = Date()
        return dateOutputWithoutFormatting.format(date)
    }

    fun getDate(date: Date): String {
        return dateOutput.format(date)
    }

    fun getDateFromString(dateString: String): String {
        val date = parseDate(dateString)
        return dateOutput.format(date)
    }

    fun getCustomDate(dateString: String): Long {
        val date = parseDate(dateString)
        return date.time
    }

    private fun parseDate(dateString: String): Date {
        val dateInput = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
        return dateInput.parse(dateString) ?: Date()
    }
}