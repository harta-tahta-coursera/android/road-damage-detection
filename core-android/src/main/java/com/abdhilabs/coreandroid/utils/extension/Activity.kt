package com.abdhilabs.coreandroid.utils.extension

import android.app.Activity
import android.content.Context
import android.graphics.Bitmap
import android.graphics.ImageDecoder
import android.net.Uri
import android.os.Build
import android.os.Environment
import android.provider.MediaStore
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.FileOutputStream
import java.io.IOException

fun Activity.toast(message: String) {
    Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
}

fun Uri.toBitmap(context: Context): Bitmap {
    return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
        val img = ImageDecoder.decodeBitmap(ImageDecoder.createSource(context.contentResolver, this))
        Bitmap.createScaledBitmap(img, 320, 320, false)
            .copy(Bitmap.Config.ARGB_8888, true)
    } else {
        MediaStore.Images.Media.getBitmap(context.contentResolver, this)
    }
}

fun AppCompatActivity.createsTempFile(bitmap: Bitmap?): File? {
    if (bitmap == null) return null
    val file = File(
        getExternalFilesDir(Environment.DIRECTORY_PICTURES), System
            .currentTimeMillis().toString() + "_image.jpg"
    )
    val bos = ByteArrayOutputStream()
    bitmap.compress(Bitmap.CompressFormat.JPEG, 50, bos)
    val bitmapData = bos.toByteArray()

    try {
        val fos = FileOutputStream(file)
        fos.apply {
            write(bitmapData)
            flush()
            close()
        }
    } catch (e: IOException) {
        e.printStackTrace()
    }
    return file
}