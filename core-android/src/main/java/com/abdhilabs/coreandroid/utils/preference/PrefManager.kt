package com.abdhilabs.coreandroid.utils.preference

import android.content.Context
import android.content.SharedPreferences

class PrefManager(context: Context) {

    private val sp: SharedPreferences by lazy {
        context.applicationContext.getSharedPreferences(PREF_NAME, Context.MODE_PRIVATE)
    }

    private val spe: SharedPreferences.Editor by lazy {
        sp.edit()
    }

    fun clear() {
        sp.edit().clear().apply()
    }

    var isLoggedIn: Boolean
        get() = sp.getBoolean(SP_IS_LOGGED_IN, false)
        set(value) = spe.putBoolean(SP_IS_LOGGED_IN, value).apply()

    var name: String
        get() = sp.getString(SP_NAME, "") ?: ""
        set(value) = spe.putString(SP_NAME, value).apply()

    var email: String
        get() = sp.getString(SP_EMAIL, "") ?: ""
        set(value) = spe.putString(SP_EMAIL, value).apply()

    var phone: String
        get() = sp.getString(SP_PHONE, "") ?: ""
        set(value) = spe.putString(SP_PHONE, value).apply()

    companion object {
        const val PREF_NAME = "com.abdhilabs.coreandroid.preference"
        const val SP_IS_LOGGED_IN = "pref_is_logged_in"
        const val SP_NAME = "pref_name"
        const val SP_EMAIL = "pref_email"
        const val SP_PHONE = "pref_phone"
    }
}