package com.abdhilabs.rodavis.presentation.news.viewholder

import com.abdhilabs.coreandroid.utils.extension.setImageFromUrl
import com.abdhilabs.rodavis.R
import com.abdhilabs.rodavis.abstraction.BaseViewHolder
import com.abdhilabs.rodavis.databinding.ItemNewsBinding
import com.abdhilabs.rodavis.domain.entity.model.Report
import com.abdhilabs.rodavis.domain.entity.ItemClickListener

class NewsViewHolder(private val binding: ItemNewsBinding): BaseViewHolder<Report>(binding) {
    companion object{
        const val LAYOUT = R.layout.item_news
    }
    override fun bind(item: Report, clickListener: ItemClickListener?) {
        setUI(binding, item)
        binding.root.setOnClickListener { clickListener?.onClick(item) }
    }

    private fun setUI(binding: ItemNewsBinding, item: Report) {
        val context = binding.root.context
        with(binding) {
            imgNews.setImageFromUrl(item.imageUrl)
            tvReporterName.text = context.getString(R.string.text_reporter_name, item.reporterName)
            tvStatus.text = item.status
            tvAddress.text = context.getString(R.string.text_reporter_location, item.address)
        }
    }
}