package com.abdhilabs.rodavis.presentation.report

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.recyclerview.widget.DividerItemDecoration
import com.abdhilabs.coreandroid.abstraction.BaseFragmentBinding
import com.abdhilabs.coreandroid.utils.extension.gone
import com.abdhilabs.coreandroid.utils.extension.hide
import com.abdhilabs.coreandroid.utils.extension.visible
import com.abdhilabs.rodavis.abstraction.BaseItemModel
import com.abdhilabs.rodavis.abstraction.BaseRecyclerViewAdapter
import com.abdhilabs.rodavis.data.factory.ItemTypeFactoryImpl
import com.abdhilabs.rodavis.data.vo.Result
import com.abdhilabs.rodavis.databinding.FragmentReportBinding
import com.abdhilabs.rodavis.domain.entity.ItemClickListener
import com.abdhilabs.rodavis.domain.entity.Shimmer
import com.abdhilabs.rodavis.domain.entity.model.History
import com.abdhilabs.rodavis.domain.entity.model.toReport
import com.abdhilabs.rodavis.presentation.detail.DetailActivity
import com.abdhilabs.rodavis.presentation.report.picture.TakePictureActivity

class ReportFragment: BaseFragmentBinding<FragmentReportBinding, ReportViewModel>(),
    ItemClickListener {

    private val adapterReportHistory by lazy { BaseRecyclerViewAdapter(ItemTypeFactoryImpl(), this) }

    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> FragmentReportBinding
        get() = FragmentReportBinding::inflate

    override fun getViewModelClass(): Class<ReportViewModel> = ReportViewModel::class.java

    override fun setupView(binding: FragmentReportBinding) {
        vm.getReportHistory()
        with(binding) {
            rvReport.adapter = adapterReportHistory
            rvReport.addItemDecoration(DividerItemDecoration(context, LinearLayout.VERTICAL))
            btnAddReport.setOnClickListener { TakePictureActivity.start(requireContext()) }
        }
        initSwipeRefresh(binding)
        observeReportHistory(binding)
    }

    private fun initSwipeRefresh(binding: FragmentReportBinding) {
//        binding.swipeLayout.setOnRefreshListener {
//            vm.getReportHistory()
//        }
    }

    private fun observeReportHistory(binding: FragmentReportBinding) {
        vm.report.observe(viewLifecycleOwner) {
            when (it) {
                is Result.Loading -> {
                    stateUI(false, binding)
//                    binding.swipeLayout.isRefreshing = true
                    adapterReportHistory.refreshItems(Shimmer.shimmerNews)
                }
                is Result.Success -> {
                    stateUI(false, binding)
//                    binding.swipeLayout.isRefreshing = false
                    adapterReportHistory.refreshItems(it.data)
                }
                is Result.Error -> {
                    stateUI(true, binding)
//                    binding.swipeLayout.isRefreshing = false
                    binding.tvReportError.text = it.errorMessage
                }
            }
        }
    }

    private fun stateUI(isRvHide: Boolean, binding: FragmentReportBinding) {
        with(binding) {
            when (isRvHide) {
                true -> {
                    rvReport.hide()
                    tvReportError.visible()
                }
                false -> {
                    rvReport.visible()
                    tvReportError.gone()
                }
            }
        }
    }

    override fun onClick(item: BaseItemModel) {
        when (item) {
            is History -> DetailActivity.start(requireContext(), item.toReport())
        }
    }
}