package com.abdhilabs.rodavis.presentation.auth

import android.app.AlertDialog
import android.view.LayoutInflater
import android.view.ViewGroup
import com.abdhilabs.coreandroid.abstraction.BaseFragmentBinding
import com.abdhilabs.coreandroid.utils.extension.gone
import com.abdhilabs.coreandroid.utils.extension.toast
import com.abdhilabs.coreandroid.utils.extension.visible
import com.abdhilabs.coreandroid.utils.preference.EncryptedPreference
import com.abdhilabs.coreandroid.utils.preference.PrefManager
import com.abdhilabs.rodavis.data.vo.Result
import com.abdhilabs.rodavis.databinding.FragmentMainBinding
import com.abdhilabs.rodavis.domain.entity.model.Login
import com.abdhilabs.rodavis.domain.entity.params.LoginParams
import com.abdhilabs.rodavis.domain.entity.params.RegisterParams
import com.abdhilabs.rodavis.presentation.main.MainActivity
import timber.log.Timber
import javax.inject.Inject

class MainFragment : BaseFragmentBinding<FragmentMainBinding, AuthViewModel>() {

    @Inject
    lateinit var pref: EncryptedPreference

    private val prefAccount by lazy { PrefManager(requireContext()) }

    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> FragmentMainBinding
        get() = FragmentMainBinding::inflate

    override fun getViewModelClass(): Class<AuthViewModel> = AuthViewModel::class.java

    override fun setupView(binding: FragmentMainBinding) {
        observeLogin(binding)
        observeRegister(binding)
        with(binding) {
            btnLogin.setOnClickListener { onButtonLoginClicked() }
            btnRegister.setOnClickListener { onButtonRegisterClicked() }
        }
    }

    private fun observeLogin(binding: FragmentMainBinding) {
        vm.login.observe(viewLifecycleOwner) {
            when (it) {
                is Result.Loading -> binding.progressBarAuth.visible()
                is Result.Success -> {
                    Timber.i("Success")
                    binding.progressBarAuth.gone()
                    saveAccount(it.data)
                    prefAccount.clear()
                    MainActivity.start(requireContext())
                    requireActivity().finish()
                }
                is Result.Error -> {
                    binding.progressBarAuth.gone()
                    if (it.code == 401) dialogMessage("Email atau Password anda salah").show()
                    else dialogMessage(it.errorMessage ?: "").show()
                }
            }
        }
    }

    private fun saveAccount(data: Login) {
        pref.secureToken = data.token
        pref.email = data.email
        pref.name = data.name
        pref.phone = data.phoneNumber
    }

    private fun observeRegister(binding: FragmentMainBinding) {
        vm.register.observe(viewLifecycleOwner) {
            when (it) {
                is Result.Loading -> binding.progressBarAuth.visible()
                is Result.Success -> {
                    binding.progressBarAuth.gone()
                    dialogMessage("Anda berhasil membuat akun baru").show()
                    prefAccount.clear()
                }
                is Result.Error -> {
                    binding.progressBarAuth.gone()
                    dialogMessage(it.errorMessage ?: "").show()
                }
            }
        }
    }

    private fun onButtonLoginClicked() {
        val loginDialog = LoginFragment()
        loginDialog.show(childFragmentManager, loginDialog.tag)
        loginDialog.setLoginListener(object : LoginFragment.LoginBottomSheetListener {
            override fun onForgotPasswordClicked() {
                toast("Forgot Password Clicked")
            }

            override fun onLoginButtonClicked(params: LoginParams) {
                vm.login(params)
                saveLoginTemporary(params)
            }

            override fun onRegisterButtonClicked() {
                onButtonRegisterClicked()
            }
        })
    }

    private fun onButtonRegisterClicked() {
        val registerDialog = RegisterFragment()
        registerDialog.show(childFragmentManager, registerDialog.tag)
        registerDialog.setRegisterListener(object : RegisterFragment.RegisterBottomSheetListener {
            override fun onRegisterButtonClicked(params: RegisterParams) {
                vm.register(params)
                saveRegisterTemporary(params)
            }

            override fun onLoginButtonClicked() {
                onButtonLoginClicked()
            }
        })
    }

    private fun dialogMessage(msg: String): AlertDialog.Builder {
        return AlertDialog.Builder(context).also {
            it.setTitle("Pesan").setMessage(msg)
            it.setPositiveButton("Oke") { dialog, _ ->
                dialog.dismiss()
            }
        }
    }

    private fun saveLoginTemporary(params: LoginParams) {
        prefAccount.apply {
            email = params.email
        }
    }

    private fun saveRegisterTemporary(params: RegisterParams) {
        prefAccount.apply {
            name = params.name
            email = params.email
            phone = params.phoneNumber
        }
    }

}