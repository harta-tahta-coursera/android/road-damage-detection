package com.abdhilabs.rodavis.presentation.report.viewholder

import com.abdhilabs.rodavis.R
import com.abdhilabs.rodavis.abstraction.BaseViewHolder
import com.abdhilabs.rodavis.databinding.ItemReportBinding
import com.abdhilabs.rodavis.domain.entity.ItemClickListener
import com.abdhilabs.rodavis.domain.entity.model.History

class ReportViewHolder(private val binding: ItemReportBinding): BaseViewHolder<History>(binding) {
    companion object{
        const val LAYOUT = R.layout.item_report
    }
    override fun bind(item: History, clickListener: ItemClickListener?) {
        setUI(binding, item)
        binding.root.setOnClickListener { clickListener?.onClick(item) }
    }

    private fun setUI(binding: ItemReportBinding, item: History) {
        val context = binding.root.context
        with(binding) {
            tvStatus.text = item.status
            tvAddress.text = context.getString(R.string.text_reporter_location, item.address)
        }
    }
}