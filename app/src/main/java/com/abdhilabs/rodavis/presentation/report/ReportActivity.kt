package com.abdhilabs.rodavis.presentation.report

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import com.abdhilabs.coreandroid.abstraction.BaseActivityBinding
import com.abdhilabs.coreandroid.utils.extension.*
import com.abdhilabs.rodavis.data.vo.Result
import com.abdhilabs.rodavis.databinding.ActivityReportBinding
import com.abdhilabs.rodavis.domain.entity.model.LocationModel
import com.abdhilabs.rodavis.presentation.report.location.PickLocationActivity
import okhttp3.MediaType
import okhttp3.MultipartBody
import okhttp3.RequestBody
import javax.inject.Inject

class ReportActivity : BaseActivityBinding<ActivityReportBinding>() {

    @Inject
    lateinit var vm: ReportViewModel

    private lateinit var location: LocationModel

    override val bindingInflater: (LayoutInflater) -> ActivityReportBinding
        get() = ActivityReportBinding::inflate

    override fun setupView(binding: ActivityReportBinding) {
        val uriImage = intent.getStringExtra(URI_IMAGE)
        val uri = Uri.parse(uriImage)
        binding.imageView.setImageBitmap(uri.toBitmap(this))
        binding.etLocation.setOnClickListener {
            startActivityForResult(
                Intent(this, PickLocationActivity::class.java),
                PICK_LOCATION_CODE
            )
        }
        binding.btnReport.setOnClickListener { createReport(uri) }
        observeAddReport()
    }

    private fun observeAddReport() {
        vm.addReport.observe(this) {
            when (it) {
                is Result.Loading -> binding.progressBar.visible()
                is Result.Success -> {
                    binding.progressBar.gone()
                    toast("Anda berhasil mengirim laporan")
                    finish()
                }
                is Result.Error -> {
                    binding.progressBar.gone()
                    toast(it.errorMessage.toString())
                }
            }
        }
    }

    private fun showResultSuccess() {

    }

    private fun createReport(uri: Uri) {
        val note = binding.etNote.text.toString()
        val map = HashMap<String, RequestBody>()
        map["lat"] = createPartFromString(location.locationLatitude)
        map["lng"] = createPartFromString(location.locationLongitude)
        map["address"] = createPartFromString(location.locationAddress)
        map["note"] = createPartFromString(note)
        map["_method"] = createPartFromString("PATCH")

        val file = createsTempFile(uri.toBitmap(this)) ?: return
        val reqFile = RequestBody.create(MediaType.parse("image/*"), file)
        val body = MultipartBody.Part.createFormData("image", file.name, reqFile)
        vm.sendReport(body, map)
    }

    private fun createPartFromString(value: String): RequestBody {
        return RequestBody.create(MediaType.parse("multipart/form-data; charset=utf-8"), value)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            PICK_LOCATION_CODE -> {
                if (resultCode == RESULT_OK) {
                    val extras =
                        data?.getParcelableExtra<LocationModel>(PickLocationActivity.LOCATION)
                    binding.etLocation.setText(extras?.locationAddress)
                    location = extras ?: return
                }
            }
        }
    }

    companion object {
        @JvmStatic
        fun start(context: Context, uri: Uri) {
            val starter = Intent(context, ReportActivity::class.java)
                .putExtra(URI_IMAGE, uri.toString())
            context.startActivity(starter)
        }

        private const val URI_IMAGE = "uri_image"
        private const val PICK_LOCATION_CODE = 202
    }
}