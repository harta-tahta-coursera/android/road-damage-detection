package com.abdhilabs.rodavis.presentation.auth

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import com.abdhilabs.coreandroid.abstraction.BaseActivityBinding
import com.abdhilabs.rodavis.databinding.ActivityAuthBinding

class AuthActivity: BaseActivityBinding<ActivityAuthBinding>() {

    companion object {
        @JvmStatic
        fun start(context: Context) {
            val starter = Intent(context, AuthActivity::class.java)
            context.startActivity(starter)
        }
    }

    override val bindingInflater: (LayoutInflater) -> ActivityAuthBinding
        get() = ActivityAuthBinding::inflate

    override fun setupView(binding: ActivityAuthBinding) {

    }
}