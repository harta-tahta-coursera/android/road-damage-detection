package com.abdhilabs.rodavis.presentation.news

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.recyclerview.widget.DividerItemDecoration
import com.abdhilabs.coreandroid.abstraction.BaseFragmentBinding
import com.abdhilabs.coreandroid.utils.extension.gone
import com.abdhilabs.coreandroid.utils.extension.hide
import com.abdhilabs.coreandroid.utils.extension.visible
import com.abdhilabs.rodavis.abstraction.BaseItemModel
import com.abdhilabs.rodavis.abstraction.BaseRecyclerViewAdapter
import com.abdhilabs.rodavis.data.factory.ItemTypeFactoryImpl
import com.abdhilabs.rodavis.data.vo.Result
import com.abdhilabs.rodavis.databinding.FragmentNewsBinding
import com.abdhilabs.rodavis.domain.entity.Shimmer
import com.abdhilabs.rodavis.domain.entity.model.Report
import com.abdhilabs.rodavis.domain.entity.ItemClickListener
import com.abdhilabs.rodavis.presentation.detail.DetailActivity

class NewsFragment : BaseFragmentBinding<FragmentNewsBinding, NewsViewModel>(), ItemClickListener {

    private val adapterNews by lazy { BaseRecyclerViewAdapter(ItemTypeFactoryImpl(), this) }

    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> FragmentNewsBinding
        get() = FragmentNewsBinding::inflate

    override fun getViewModelClass(): Class<NewsViewModel> = NewsViewModel::class.java

    override fun setupView(binding: FragmentNewsBinding) {
        vm.getNews()
        with(binding) {
            rvNews.adapter = adapterNews
            rvNews.addItemDecoration(DividerItemDecoration(context, LinearLayout.VERTICAL))
        }
        observeNews(binding)
        initSwipeRefresh(binding)
    }

    private fun initSwipeRefresh(binding: FragmentNewsBinding) {
        binding.swipeLayout.setOnRefreshListener {
            vm.getNews()
        }
    }

    private fun observeNews(binding: FragmentNewsBinding) {
        vm.report.observe(viewLifecycleOwner) {
            when (it) {
                is Result.Loading -> {
                    stateUI(false, binding)
                    binding.swipeLayout.isRefreshing = true
                    adapterNews.refreshItems(Shimmer.shimmerNews)
                }
                is Result.Success -> {
                    stateUI(false, binding)
                    binding.swipeLayout.isRefreshing = false
                    adapterNews.refreshItems(it.data)
                }
                is Result.Error -> {
                    stateUI(true, binding)
                    binding.swipeLayout.isRefreshing = false
                    binding.tvNewsError.text = it.errorMessage
                }
            }
        }
    }

    private fun stateUI(isRvHide: Boolean, binding: FragmentNewsBinding) {
        with(binding) {
            when (isRvHide) {
                true -> {
                    rvNews.hide()
                    tvNewsError.visible()
                }
                false -> {
                    rvNews.visible()
                    tvNewsError.gone()
                }
            }
        }
    }

    override fun onClick(item: BaseItemModel) {
        when (item) {
            is Report -> {
                DetailActivity.start(requireContext(), item)
            }
        }
    }
}