package com.abdhilabs.rodavis.presentation.news

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.abdhilabs.coreandroid.abstraction.UseCase
import com.abdhilabs.rodavis.data.vo.Result
import com.abdhilabs.rodavis.domain.entity.model.Report
import com.abdhilabs.rodavis.domain.usecase.GetNewsUseCase
import kotlinx.coroutines.launch
import javax.inject.Inject

class NewsViewModel @Inject constructor(
    private val getNewsUseCase: GetNewsUseCase
) : ViewModel() {

    private val _news = MutableLiveData<Result<List<Report>>>()
    val report: LiveData<Result<List<Report>>>
        get() = _news

    fun getNews() {
        _news.value = Result.Loading
        viewModelScope.launch {
            _news.value = getNewsUseCase(UseCase.None)
        }
    }
}