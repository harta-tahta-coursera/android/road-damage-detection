package com.abdhilabs.rodavis.presentation.detail

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import com.abdhilabs.coreandroid.abstraction.BaseActivityBinding
import com.abdhilabs.coreandroid.utils.extension.setImageFromUrl
import com.abdhilabs.coreandroid.utils.formatter.DateTimeFormatter
import com.abdhilabs.rodavis.R
import com.abdhilabs.rodavis.databinding.ActivityDetailBinding
import com.abdhilabs.rodavis.domain.entity.model.Report

class DetailActivity : BaseActivityBinding<ActivityDetailBinding>() {

    private lateinit var item: Report

    companion object {
        @JvmStatic
        fun start(context: Context, report: Report) {
            val starter = Intent(context, DetailActivity::class.java)
                .putExtra(DETAIL_REPORT, report)
            context.startActivity(starter)
        }

        const val DETAIL_REPORT = "detail_report"
    }

    override val bindingInflater: (LayoutInflater) -> ActivityDetailBinding
        get() = ActivityDetailBinding::inflate

    override fun setupView(binding: ActivityDetailBinding) {
        supportActionBar?.title = "Detail Laporan"
        item = intent.getParcelableExtra(DETAIL_REPORT) ?: return
        setUi(item)
    }

    private fun setUi(item: Report) {
        with(binding) {
            imgReport.setImageFromUrl(item.imageUrl)
            tvAddress.text = getString(R.string.text_reporter_location, item.address)
            tvDate.text = getString(
                R.string.text_reporter_date,
                DateTimeFormatter.getDateFromString(item.dateReported)
            )
            tvReporterName.text = getString(R.string.text_reporter_name, item.reporterName)
            tvStatus.text = item.status
            tvType.text = getString(
                R.string.text_reporter_type,
                mappingTypeDamage(item.classes).joinToString(" | ")
            )
        }
    }

    private fun mappingTypeDamage(item: List<String>): List<String> {
        val types = mutableListOf<String>()
        val map = hashMapOf(
            "D00" to getString(R.string.text_d00),
            "D01" to getString(R.string.text_d01),
            "D10" to getString(R.string.text_d10),
            "D11" to getString(R.string.text_d11),
            "D20" to getString(R.string.text_d20),
            "D40" to getString(R.string.text_d40),
            "D43" to getString(R.string.text_d43),
            "D44" to getString(R.string.text_d44),
            "D50" to getString(R.string.text_d50)
        )
        for (i in item) {
            if (!map[i].isNullOrBlank()) types.add(map[i].toString())
        }
        return types
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_detail, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.item_maps -> openMaps()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun openMaps() {
        val gmmIntentUri =
            Uri.parse("geo:${item.location.lat},${item.location.lng}")
        val mapIntent = Intent(Intent.ACTION_VIEW, gmmIntentUri)
        mapIntent.setPackage("com.google.android.apps.maps")
        mapIntent.resolveActivity(packageManager)?.let {
            startActivity(mapIntent)
        }
    }
}