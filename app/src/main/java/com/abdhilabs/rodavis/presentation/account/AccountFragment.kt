package com.abdhilabs.rodavis.presentation.account

import android.view.LayoutInflater
import android.view.ViewGroup
import com.abdhilabs.coreandroid.abstraction.BaseFragmentBinding
import com.abdhilabs.coreandroid.utils.preference.EncryptedPreference
import com.abdhilabs.rodavis.databinding.FragmentAccountBinding
import com.abdhilabs.rodavis.presentation.auth.AuthActivity
import javax.inject.Inject

class AccountFragment: BaseFragmentBinding<FragmentAccountBinding, AccountViewModel>() {

    @Inject
    lateinit var pref: EncryptedPreference

    override val bindingInflater: (LayoutInflater, ViewGroup?, Boolean) -> FragmentAccountBinding
        get() = FragmentAccountBinding::inflate

    override fun getViewModelClass(): Class<AccountViewModel> = AccountViewModel::class.java

    override fun setupView(binding: FragmentAccountBinding) {
        setUI(binding)
        binding.btnLogout.setOnClickListener {
            pref.clear()
            AuthActivity.start(requireContext())
            requireActivity().finish()
        }
    }

    private fun setUI(binding: FragmentAccountBinding) {
        with(binding) {
            tvEmail.text = pref.email
            tvName.text = pref.name
            tvPhone.text = pref.phone
        }
    }
}