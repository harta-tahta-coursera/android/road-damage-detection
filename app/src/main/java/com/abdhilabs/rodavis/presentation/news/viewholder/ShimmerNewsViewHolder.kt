package com.abdhilabs.rodavis.presentation.news.viewholder

import com.abdhilabs.rodavis.R
import com.abdhilabs.rodavis.abstraction.BaseViewHolder
import com.abdhilabs.rodavis.databinding.ItemNewsShimmerBinding
import com.abdhilabs.rodavis.domain.entity.ShimmerNews
import com.abdhilabs.rodavis.domain.entity.ItemClickListener

class ShimmerNewsViewHolder(binding: ItemNewsShimmerBinding) :
    BaseViewHolder<ShimmerNews>(binding) {
    companion object {
        const val LAYOUT = R.layout.item_news_shimmer
    }

    override fun bind(item: ShimmerNews, clickListener: ItemClickListener?) {

    }
}