package com.abdhilabs.rodavis.presentation.auth

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.doOnTextChanged
import com.abdhilabs.coreandroid.utils.extension.isValidEmailFormat
import com.abdhilabs.coreandroid.utils.preference.EncryptedPreference
import com.abdhilabs.coreandroid.utils.preference.PrefManager
import com.abdhilabs.rodavis.databinding.FragmentLoginBinding
import com.abdhilabs.rodavis.domain.entity.params.LoginParams
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import dagger.android.support.AndroidSupportInjection
import timber.log.Timber
import javax.inject.Inject

class LoginFragment : BottomSheetDialogFragment() {

    @Inject
    lateinit var pref: EncryptedPreference

    private val prefAccount by lazy { PrefManager(requireContext()) }

    private lateinit var binding: FragmentLoginBinding
    private var listener: LoginBottomSheetListener? = null

    override fun onAttach(context: Context) {
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentLoginBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initTextWatcher()

        with(binding) {
            setValue(binding)
            btnLogin.setOnClickListener { confirmationLoginParams() }
            tvForgotPassword.setOnClickListener {
                listener?.onForgotPasswordClicked()
                dismiss()
            }
            tvNotHaveAccount.setOnClickListener {
                listener?.onRegisterButtonClicked()
                dismiss()
            }
        }
    }

    private fun setValue(binding: FragmentLoginBinding) {
        with(binding) {
            etEmail.setText(prefAccount.email)
        }
    }

    private fun initTextWatcher() {
        with(binding) {
            etEmail.doOnTextChanged { text, _, _, _ ->
                if (!text.isNullOrEmpty()) etEmail.error = null
            }
            etPassword.doOnTextChanged { text, _, _, _ ->
                if (!text.isNullOrEmpty()) etPassword.error = null
            }
        }
    }

    private fun confirmationLoginParams() {
        with(binding) {
            if (!etEmail.isValidEmailFormat()) return
            val email = etEmail.text.toString()
            val password = etPassword.text.toString()

            if (email.isEmpty() || password.isEmpty()) {
                when {
                    email.isEmpty() -> etEmail.error = "Isi email anda terlebih dahulu"
                    password.isEmpty() -> etPassword.error = "Isi password anda terlebih dahulu"
                }
            } else {
                val params =
                    LoginParams(
                        email = email,
                        password = password,
                    )
                Timber.i("User: $params")
                listener?.onLoginButtonClicked(params)
                dismiss()
            }
        }
    }

    fun setLoginListener(listener: LoginBottomSheetListener) {
        this.listener = listener
    }

    interface LoginBottomSheetListener {
        fun onForgotPasswordClicked()
        fun onLoginButtonClicked(params: LoginParams)
        fun onRegisterButtonClicked()
    }

}