package com.abdhilabs.rodavis.presentation.splash

import android.content.Intent
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import com.abdhilabs.coreandroid.abstraction.BaseActivityBinding
import com.abdhilabs.coreandroid.utils.preference.EncryptedPreference
import com.abdhilabs.rodavis.databinding.ActivitySplashBinding
import com.abdhilabs.rodavis.presentation.auth.AuthActivity
import com.abdhilabs.rodavis.presentation.main.MainActivity
import javax.inject.Inject

class SplashActivity : BaseActivityBinding<ActivitySplashBinding>() {

    @Inject
    lateinit var pref: EncryptedPreference

    override val bindingInflater: (LayoutInflater) -> ActivitySplashBinding
        get() = ActivitySplashBinding::inflate

    override fun setupView(binding: ActivitySplashBinding) {
        moveToMain()
    }

    private fun moveToMain() {
        Handler(Looper.getMainLooper()).postDelayed({
            if (pref.secureToken.isNullOrEmpty()) {
                startActivity(Intent(this, AuthActivity::class.java))
                finish()
            } else {
                startActivity(Intent(this, MainActivity::class.java))
                finish()
            }
        }, 2000)
    }
}