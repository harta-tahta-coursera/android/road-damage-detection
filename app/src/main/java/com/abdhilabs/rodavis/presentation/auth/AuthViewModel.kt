package com.abdhilabs.rodavis.presentation.auth

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.abdhilabs.rodavis.domain.entity.model.Login
import com.abdhilabs.rodavis.domain.entity.model.Register
import com.abdhilabs.rodavis.domain.entity.params.RegisterParams
import kotlinx.coroutines.launch
import javax.inject.Inject
import com.abdhilabs.rodavis.data.vo.Result
import com.abdhilabs.rodavis.domain.entity.params.LoginParams
import com.abdhilabs.rodavis.domain.usecase.PostLoginUseCase
import com.abdhilabs.rodavis.domain.usecase.PostRegisterUseCase

class AuthViewModel @Inject constructor(
    private val postRegisterUseCase: PostRegisterUseCase,
    private val postLoginUseCase: PostLoginUseCase
): ViewModel() {

    private val _register = MutableLiveData<Result<Register>>()
    val register: LiveData<Result<Register>> get() = _register

    private val _login = MutableLiveData<Result<Login>>()
    val login: LiveData<Result<Login>> get() = _login

    fun register(params: RegisterParams) {
        viewModelScope.launch {
            _register.value = Result.Loading
            _register.value = postRegisterUseCase(params)
        }
    }

    fun login(params: LoginParams) {
        viewModelScope.launch {
            _login.value = Result.Loading
            _login.value = postLoginUseCase(params)
        }
    }
}