package com.abdhilabs.rodavis.presentation.auth

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.doOnTextChanged
import com.abdhilabs.coreandroid.utils.preference.PrefManager
import com.abdhilabs.rodavis.databinding.FragmentRegisterBinding
import com.abdhilabs.rodavis.domain.entity.params.RegisterParams
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

class RegisterFragment : BottomSheetDialogFragment() {

    private val prefAccount by lazy { PrefManager(requireContext()) }

    private lateinit var binding: FragmentRegisterBinding
    private var listener: RegisterBottomSheetListener? = null

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentRegisterBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initTextWatcher()

        with(binding) {
            setValue(binding)
            btnRegister.setOnClickListener { confirmationParamsRegister() }
            tvNotHaveAccount.setOnClickListener {
                listener?.onLoginButtonClicked()
                dismiss()
            }
        }
    }

    private fun setValue(binding: FragmentRegisterBinding) {
        with(binding) {
            etName.setText(prefAccount.name)
            etEmail.setText(prefAccount.email)
            etPhoneNumber.setText(prefAccount.phone)
        }
    }

    private fun initTextWatcher() {
        with(binding) {
            etName.doOnTextChanged { text, _, _, _ ->
                if (!text.isNullOrEmpty()) etName.error = null
            }
            etEmail.doOnTextChanged { text, _, _, _ ->
                if (!text.isNullOrEmpty()) etEmail.error = null
            }
            etPassword.doOnTextChanged { text, _, _, _ ->
                if (!text.isNullOrEmpty()) etPassword.error = null
            }
            etRepeatPassword.doOnTextChanged { txt, _, _, _ ->
                val password = etPassword.text.toString()
                if (txt.toString() == password) etRepeatPassword.error = null
                else etRepeatPassword.error = "Password tidak sama"
            }
            etPhoneNumber.doOnTextChanged { text, _, _, _ ->
                if (!text.isNullOrEmpty()) etPhoneNumber.error = null
            }
        }
    }

    private fun confirmationParamsRegister() {
        with(binding) {
            val name = etName.text.toString()
            val email = etEmail.text.toString()
            val phone = etPhoneNumber.text.toString()
            val password = etPassword.text.toString()
            val rePassword = etRepeatPassword.text.toString()

            if (name.isEmpty() || email.isEmpty() || phone.isEmpty() || password.isEmpty() || rePassword.isEmpty()) {
                when {
                    name.isEmpty() -> etName.error = "Isi nama anda terlebih dahulu"
                    email.isEmpty() -> etEmail.error = "Isi email anda terlebih dahulu"
                    phone.isEmpty() -> etPhoneNumber.error = "Isi nomer hp anda terlebih dahulu"
                    password.isEmpty() -> etPassword.error = "Isi password anda terlebih dahulu"
                    rePassword.isEmpty() -> etRepeatPassword.error =
                        "Ulangi password anda terlebih dahulu"
                }
            } else {
                val params = RegisterParams(
                    name = name,
                    email = email,
                    password = password,
                    phoneNumber = phone
                )
                listener?.onRegisterButtonClicked(params)
                dismiss()
            }
        }
    }

    fun setRegisterListener(listener: RegisterBottomSheetListener) {
        this.listener = listener
    }

    interface RegisterBottomSheetListener {
        fun onRegisterButtonClicked(params: RegisterParams)
        fun onLoginButtonClicked()
    }

}