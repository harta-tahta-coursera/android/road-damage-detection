package com.abdhilabs.rodavis.presentation.report

import android.app.Application
import android.graphics.*
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.abdhilabs.coreandroid.abstraction.UseCase
import com.abdhilabs.rodavis.data.vo.Result
import com.abdhilabs.rodavis.domain.entity.model.AddReport
import com.abdhilabs.rodavis.domain.entity.model.DetectionResult
import com.abdhilabs.rodavis.domain.entity.model.History
import com.abdhilabs.rodavis.domain.usecase.GetReportHistoryUseCase
import com.abdhilabs.rodavis.domain.usecase.PostAddReportUseCase
import kotlinx.coroutines.launch
import okhttp3.MultipartBody
import okhttp3.RequestBody
import javax.inject.Inject

class ReportViewModel @Inject constructor(
    application: Application,
    private val postAddReportUseCase: PostAddReportUseCase,
    private val getReportHistoryUseCase: GetReportHistoryUseCase
) : AndroidViewModel(application) {

    private val _addReport = MutableLiveData<Result<AddReport>>()
    val addReport: LiveData<Result<AddReport>>
        get() = _addReport

    private val _report = MutableLiveData<Result<List<History>>>()
    val report: LiveData<Result<List<History>>>
        get() = _report

    fun sendReport(
        photo: MultipartBody.Part,
        map: HashMap<String, RequestBody>
    ) {
        val params = PostAddReportUseCase.Params(photo, map)
        _addReport.value = Result.Loading
        viewModelScope.launch {
            _addReport.value = postAddReportUseCase(params)
        }
    }

    fun getReportHistory() {
        _report.value = Result.Loading
        viewModelScope.launch {
            _report.value = getReportHistoryUseCase(UseCase.None)
        }
    }

    companion object {
        private const val MAX_FONT_SIZE = 96F
    }

//    fun setImage(src: Bitmap): Bitmap {
//        val bitmap = Bitmap.createScaledBitmap(src, 300, 300, true)
//        val tensorImage = TensorImage(DataType.FLOAT32)
//        tensorImage.load(bitmap)
//        val byteBuffer = tensorImage.buffer
//        return getOutput(byteBuffer, bitmap)
//    }

//    private fun getOutput(byteBuffer: ByteBuffer, bitmap: Bitmap): Bitmap {
//        val model = Detect.newInstance(getApplication())
//
//        // Creates inputs for reference.
//        val inputFeature0 =
//            TensorBuffer.createFixedSize(intArrayOf(1, 300, 300, 3), DataType.FLOAT32)
//        inputFeature0.loadBuffer(byteBuffer)
//
//        // Runs model inference and gets result.
//        val outputs = model.process(inputFeature0)
//        val outputLocations = outputs.outputFeature0AsTensorBuffer //Bound Box
//        val outputClasses = outputs.outputFeature1AsTensorBuffer //Class
//        val outputScores = outputs.outputFeature2AsTensorBuffer //Probabilitas
//        val outputDetections = outputs.outputFeature3AsTensorBuffer //Amount detection
//
//        Timber.i("Size Location: ${outputLocations.floatArray.size}")
//        Timber.i("Size Scores: ${outputScores.floatArray.size}")
//        Timber.i("Size Detection: ${outputDetections.floatArray.size}")
//
//        val location = outputLocations.floatArray
//
//        val detection = DetectionResult(
//            RectF(location[0], location[1], location[2], location[3]),
//            outputClasses.intArray[0].toString()
//        )
//
//
//        return drawDetectionResult(bitmap, listOf(detection))
//    }

    private fun drawDetectionResult(
        bitmap: Bitmap,
        detectionResults: List<DetectionResult>
    ): Bitmap {
        val outputBitmap = bitmap.copy(Bitmap.Config.ARGB_8888, true)
        val canvas = Canvas(outputBitmap)
        val pen = Paint()
        pen.textAlign = Paint.Align.LEFT

        detectionResults.forEach {
            // draw bounding box
            pen.color = Color.RED
            pen.strokeWidth = 8F
            pen.style = Paint.Style.STROKE
            val box = it.boundingBox
            canvas.drawRect(box, pen)


            val tagSize = Rect(0, 0, 0, 0)

            // calculate the right font size
            pen.style = Paint.Style.FILL_AND_STROKE
            pen.color = Color.YELLOW
            pen.strokeWidth = 2F

            pen.textSize = MAX_FONT_SIZE
            pen.getTextBounds(it.text, 0, it.text.length, tagSize)
            val fontSize: Float = pen.textSize * box.width() / tagSize.width()

            // adjust the font size so texts are inside the bounding box
            if (fontSize < pen.textSize) pen.textSize = fontSize

            var margin = (box.width() - tagSize.width()) / 2.0F
            if (margin < 0F) margin = 0F
            canvas.drawText(
                it.text, box.left + margin,
                box.top + tagSize.height().times(1F), pen
            )
        }

        return outputBitmap
    }
}