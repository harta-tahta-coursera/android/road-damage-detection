package com.abdhilabs.rodavis.data.vo

enum class HttpResult {
    NO_CONNECTION,
    UNAUTHENTICATED,
    TIMEOUT,
    CLIENT_ERROR,
    BAD_RESPONSE,
    SERVER_ERROR,
    NOT_DEFINED,
}