package com.abdhilabs.rodavis.data.source.remote

import com.abdhilabs.rodavis.data.source.remote.service.AuthService
import com.abdhilabs.rodavis.data.source.RemoteDataSource
import com.abdhilabs.rodavis.data.source.remote.response.AuthResponse
import com.abdhilabs.rodavis.domain.entity.params.RegisterParams
import kotlinx.coroutines.CoroutineDispatcher
import javax.inject.Inject
import com.abdhilabs.rodavis.data.vo.Result
import com.abdhilabs.rodavis.domain.entity.params.LoginParams


class AuthRemoteDataSource @Inject constructor(private val service: AuthService) :
    RemoteDataSource() {

    suspend fun register(dispatcher: CoroutineDispatcher, params: RegisterParams): Result<AuthResponse> {
        return safeApiCall(dispatcher) { service.register(params) }
    }

    suspend fun login(dispatcher: CoroutineDispatcher, params: LoginParams): Result<AuthResponse> {
        return safeApiCall(dispatcher) { service.login(params) }
    }

}