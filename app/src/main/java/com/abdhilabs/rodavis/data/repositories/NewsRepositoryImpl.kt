package com.abdhilabs.rodavis.data.repositories

import com.abdhilabs.rodavis.data.dispatcher.CoroutineDispatcherProvider
import com.abdhilabs.rodavis.data.mapper.ReportMapper
import com.abdhilabs.rodavis.data.source.remote.NewsRemoteDataSource
import com.abdhilabs.rodavis.data.vo.Result
import com.abdhilabs.rodavis.domain.entity.model.Report
import com.abdhilabs.rodavis.domain.repository.NewsRepository
import javax.inject.Inject

class NewsRepositoryImpl @Inject constructor(
    private val remote: NewsRemoteDataSource,
    private val dispatcher: CoroutineDispatcherProvider,
    private val reportMapper: ReportMapper
): NewsRepository {

    override suspend fun getNews(): Result<List<Report>> {
        return when (val apiResult = remote.news(dispatcher.io)) {
            is Result.Success -> Result.Success(reportMapper.map(apiResult.data))
            is Result.Error -> Result.Error(apiResult.cause, apiResult.code, apiResult.errorMessage)
            else -> Result.Error()
        }
    }
}