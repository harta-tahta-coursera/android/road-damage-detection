package com.abdhilabs.rodavis.data.mapper

import com.abdhilabs.coreandroid.abstraction.Mapper
import com.abdhilabs.rodavis.data.source.remote.response.AuthResponse
import com.abdhilabs.rodavis.domain.entity.model.Register
import javax.inject.Inject

class RegisterMapper @Inject constructor() : Mapper<AuthResponse, Register>() {
    override fun map(input: AuthResponse): Register {
        return Register(
            input.message ?: "",
            input.status ?: 0
        )
    }
}