package com.abdhilabs.rodavis.data.mapper

import com.abdhilabs.coreandroid.abstraction.Mapper
import com.abdhilabs.rodavis.data.source.remote.response.AuthResponse
import com.abdhilabs.rodavis.domain.entity.model.Login
import javax.inject.Inject

class LoginMapper @Inject constructor() : Mapper<AuthResponse, Login>() {
    override fun map(input: AuthResponse): Login {
        val it = input.data
        return Login(
            it?.token ?: "",
            it?.user?.email ?: "",
            it?.user?.name ?: "",
            it?.user?.phoneNumber ?: ""
        )
    }
}