package com.abdhilabs.rodavis.data.mapper

import com.abdhilabs.coreandroid.abstraction.Mapper
import com.abdhilabs.rodavis.data.source.remote.response.ReportResponse
import com.abdhilabs.rodavis.domain.entity.model.History
import javax.inject.Inject

class HistoryMapper @Inject constructor() : Mapper<ReportResponse, List<History>>() {
    override fun map(input: ReportResponse): List<History> {
        return input.data?.map {
            History(
                it.dateReported ?: "",
                it.id ?: 0,
                it.imageUrl ?: "",
                it.address ?: "",
                location = mappingLocation(it.location),
                it.note ?: "",
                it.reporterName ?: "",
                it.status ?: "",
                it.classes ?: listOf()
            )
        } ?: emptyList()
    }

    private fun mappingLocation(location: ReportResponse.Location?): History.Location {
        return History.Location(location?.lat ?: 0.0, location?.lng ?: 0.0)
    }
}