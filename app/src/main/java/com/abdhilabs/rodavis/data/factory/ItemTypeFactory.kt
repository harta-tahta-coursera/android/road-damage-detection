package com.abdhilabs.rodavis.data.factory

import android.view.View
import android.view.ViewGroup
import com.abdhilabs.rodavis.domain.entity.ShimmerNews
import com.abdhilabs.rodavis.domain.entity.model.History
import com.abdhilabs.rodavis.domain.entity.model.Report

interface ItemTypeFactory {
    //    fun type(error: Error): Int
    fun type(shimmer: ShimmerNews): Int
    fun type(report: Report): Int
    fun type(history: History): Int
    fun createViewHolder(parent: View, viewGroup: ViewGroup, type: Int): com.abdhilabs.rodavis.abstraction.BaseViewHolder<*>
}