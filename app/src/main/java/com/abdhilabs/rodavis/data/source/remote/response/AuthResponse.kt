package com.abdhilabs.rodavis.data.source.remote.response

import com.squareup.moshi.Json

data class AuthResponse(
    @Json(name = "data") val data: Data? = null,
    @Json(name = "message") val message: String? = null,
    @Json(name = "status") val status: Int? = null
) {
    data class Data(
        @Json(name = "token") val token: String? = null,
        @Json(name = "user") val user: User? = null
    ) {
        data class User(
            @Json(name = "createdAt") val createdAt: String? = null,
            @Json(name = "email") val email: String? = null,
            @Json(name = "id") val id: Int? = null,
            @Json(name = "name") val name: String? = null,
            @Json(name = "phoneNumber") val phoneNumber: String? = null,
            @Json(name = "updatedAt") val updatedAt: String? = null
        )
    }
}