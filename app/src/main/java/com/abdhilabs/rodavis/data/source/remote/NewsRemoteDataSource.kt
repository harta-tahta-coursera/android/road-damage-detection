package com.abdhilabs.rodavis.data.source.remote

import com.abdhilabs.rodavis.data.source.RemoteDataSource
import com.abdhilabs.rodavis.data.source.remote.response.ReportResponse
import com.abdhilabs.rodavis.data.source.remote.service.ReportService
import com.abdhilabs.rodavis.data.vo.Result
import kotlinx.coroutines.CoroutineDispatcher
import javax.inject.Inject


class NewsRemoteDataSource @Inject constructor(private val service: ReportService) :
    RemoteDataSource() {

    suspend fun news(dispatcher: CoroutineDispatcher): Result<ReportResponse> {
        return safeApiCall(dispatcher) { service.reports() }
    }
}