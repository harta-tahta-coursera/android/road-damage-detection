package com.abdhilabs.rodavis.data.repositories

import com.abdhilabs.rodavis.data.dispatcher.CoroutineDispatcherProvider
import com.abdhilabs.rodavis.data.mapper.AddReportMapper
import com.abdhilabs.rodavis.data.mapper.HistoryMapper
import com.abdhilabs.rodavis.data.source.remote.ReportRemoteDataSource
import com.abdhilabs.rodavis.data.vo.Result
import com.abdhilabs.rodavis.domain.entity.model.AddReport
import com.abdhilabs.rodavis.domain.entity.model.History
import com.abdhilabs.rodavis.domain.repository.ReportRepository
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.util.*
import javax.inject.Inject

class ReportRepositoryImpl @Inject constructor(
    private val remote: ReportRemoteDataSource,
    private val dispatcher: CoroutineDispatcherProvider,
    private val historyMapper: HistoryMapper,
    private val addReportMapper: AddReportMapper
) : ReportRepository {
    override suspend fun addReport(
        photo: MultipartBody.Part,
        map: HashMap<String, RequestBody>
    ): Result<AddReport> {
        return when (val apiResult = remote.addReport(dispatcher.io, photo, map)) {
            is Result.Success -> Result.Success(addReportMapper.map(apiResult.data))
            is Result.Error -> Result.Error(apiResult.cause, apiResult.code, apiResult.errorMessage)
            else -> Result.Error()
        }
    }

    override suspend fun getReportHistory(): Result<List<History>> {
        return when (val apiResult = remote.reportHistory(dispatcher.io)) {
            is Result.Success -> Result.Success(historyMapper.map(apiResult.data))
            is Result.Error -> Result.Error(apiResult.cause, apiResult.code, apiResult.errorMessage)
            else -> Result.Error()
        }
    }
}