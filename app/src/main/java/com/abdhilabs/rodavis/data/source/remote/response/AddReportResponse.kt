package com.abdhilabs.rodavis.data.source.remote.response

import androidx.annotation.Keep
import com.squareup.moshi.Json

@Keep
data class AddReportResponse(
    @Json(name = "data") val data: Data? = null,
    @Json(name = "message") val message: String? = null,
    @Json(name = "status") val status: Int? = null
) {
    data class Data(
        @Json(name = "address") val address: String? = null,
        @Json(name = "classes") val classes: List<String>? = null,
        @Json(name = "dateReported") val dateReported: String? = null,
        @Json(name = "id") val id: Int? = null,
        @Json(name = "imageUrl") val imageUrl: String? = null,
        @Json(name = "location") val location: Location? = null,
        @Json(name = "note") val note: String? = null,
        @Json(name = "reporterName") val reporterName: String? = null,
        @Json(name = "status") val status: String? = null
    ) {
        data class Location(
            @Json(name = "lat") val lat: Double? = null,
            @Json(name = "lng") val lng: Double? = null
        )
    }
}