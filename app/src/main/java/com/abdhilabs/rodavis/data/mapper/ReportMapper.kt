package com.abdhilabs.rodavis.data.mapper

import com.abdhilabs.coreandroid.abstraction.Mapper
import com.abdhilabs.rodavis.data.source.remote.response.ReportResponse
import com.abdhilabs.rodavis.domain.entity.model.Report
import javax.inject.Inject

class ReportMapper @Inject constructor() : Mapper<ReportResponse, List<Report>>() {
    override fun map(input: ReportResponse): List<Report> {
        return input.data?.map {
            Report(
                it.dateReported ?: "",
                it.id ?: 0,
                it.imageUrl ?: "",
                it.address ?: "",
                location = mappingLocation(it.location),
                it.note ?: "",
                it.reporterName ?: "",
                it.status ?: "",
                it.classes ?: listOf()
            )
        } ?: emptyList()
    }

    private fun mappingLocation(location: ReportResponse.Location?): Report.Location {
        return Report.Location(location?.lat ?: 0.0, location?.lng ?: 0.0)
    }
}