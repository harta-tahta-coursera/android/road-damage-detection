package com.abdhilabs.rodavis.data.factory

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.abdhilabs.rodavis.abstraction.BaseViewHolder
import com.abdhilabs.rodavis.databinding.ItemNewsBinding
import com.abdhilabs.rodavis.databinding.ItemNewsShimmerBinding
import com.abdhilabs.rodavis.databinding.ItemReportBinding
import com.abdhilabs.rodavis.domain.entity.ShimmerNews
import com.abdhilabs.rodavis.domain.entity.model.History
import com.abdhilabs.rodavis.domain.entity.model.Report
import com.abdhilabs.rodavis.presentation.news.viewholder.NewsViewHolder
import com.abdhilabs.rodavis.presentation.news.viewholder.ShimmerNewsViewHolder
import com.abdhilabs.rodavis.presentation.report.viewholder.ReportViewHolder

class ItemTypeFactoryImpl : ItemTypeFactory {

    override fun type(shimmer: ShimmerNews): Int {
        return ShimmerNewsViewHolder.LAYOUT
    }

    override fun type(report: Report): Int {
        return NewsViewHolder.LAYOUT
    }

    override fun type(history: History): Int {
        return ReportViewHolder.LAYOUT
    }

    override fun createViewHolder(
        parent: View,
        viewGroup: ViewGroup,
        type: Int
    ): BaseViewHolder<*> {
        return when (type) {
            ShimmerNewsViewHolder.LAYOUT -> {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ItemNewsShimmerBinding.inflate(layoutInflater, viewGroup, false)
                ShimmerNewsViewHolder(binding)
            }
            NewsViewHolder.LAYOUT -> {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ItemNewsBinding.inflate(layoutInflater, viewGroup, false)
                NewsViewHolder(binding)
            }
            ReportViewHolder.LAYOUT -> {
                val layoutInflater = LayoutInflater.from(parent.context)
                val binding = ItemReportBinding.inflate(layoutInflater, viewGroup, false)
                ReportViewHolder(binding)
            }
            else -> throw IllegalArgumentException("Unknown Type")
        }
    }
}