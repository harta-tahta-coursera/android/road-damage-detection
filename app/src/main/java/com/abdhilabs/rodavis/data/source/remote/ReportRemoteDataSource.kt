package com.abdhilabs.rodavis.data.source.remote

import com.abdhilabs.rodavis.data.source.RemoteDataSource
import com.abdhilabs.rodavis.data.source.remote.response.AddReportResponse
import com.abdhilabs.rodavis.data.source.remote.response.ReportResponse
import com.abdhilabs.rodavis.data.source.remote.service.ReportService
import com.abdhilabs.rodavis.data.vo.Result
import kotlinx.coroutines.CoroutineDispatcher
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.util.HashMap
import javax.inject.Inject

class ReportRemoteDataSource @Inject constructor(private val service: ReportService) :
    RemoteDataSource() {

    suspend fun addReport(
        dispatcher: CoroutineDispatcher,
        photo: MultipartBody.Part,
        map: HashMap<String, RequestBody>
    ): Result<AddReportResponse> {
        return safeApiCall(dispatcher) { service.report(photo, map) }
    }

    suspend fun reportHistory(dispatcher: CoroutineDispatcher): Result<ReportResponse> {
        return safeApiCall(dispatcher) { service.reportHistory() }
    }
}