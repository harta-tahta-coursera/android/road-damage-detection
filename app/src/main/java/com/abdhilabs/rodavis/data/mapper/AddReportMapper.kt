package com.abdhilabs.rodavis.data.mapper

import com.abdhilabs.coreandroid.abstraction.Mapper
import com.abdhilabs.rodavis.data.source.remote.response.AddReportResponse
import com.abdhilabs.rodavis.domain.entity.model.AddReport
import javax.inject.Inject

class AddReportMapper @Inject constructor() : Mapper<AddReportResponse, AddReport>() {
    override fun map(input: AddReportResponse): AddReport {
        val it = input.data
        return AddReport(
            it?.dateReported ?: "",
            it?.classes ?: listOf(),
            it?.imageUrl ?: "",
            it?.id ?: 0,
            it?.imageUrl ?: "",
            mappingLocation(it?.location),
            it?.note ?: "",
            it?.reporterName ?: "",
            it?.status ?: ""
        )
    }

    private fun mappingLocation(location: AddReportResponse.Data.Location?): AddReport.Location {
        return AddReport.Location(location?.lat ?: 0.0, location?.lng ?: 0.0)
    }
}