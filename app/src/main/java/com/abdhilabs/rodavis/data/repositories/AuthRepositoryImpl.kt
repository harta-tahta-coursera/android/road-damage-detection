package com.abdhilabs.rodavis.data.repositories

import com.abdhilabs.rodavis.data.dispatcher.CoroutineDispatcherProvider
import com.abdhilabs.rodavis.data.mapper.LoginMapper
import com.abdhilabs.rodavis.data.mapper.RegisterMapper
import com.abdhilabs.rodavis.data.source.remote.AuthRemoteDataSource
import com.abdhilabs.rodavis.data.vo.Result
import com.abdhilabs.rodavis.domain.entity.model.Login
import com.abdhilabs.rodavis.domain.entity.model.Register
import com.abdhilabs.rodavis.domain.entity.params.LoginParams
import com.abdhilabs.rodavis.domain.entity.params.RegisterParams
import com.abdhilabs.rodavis.domain.repository.AuthRepository
import javax.inject.Inject

class AuthRepositoryImpl @Inject constructor(
    private val remote: AuthRemoteDataSource,
    private val dispatcher: CoroutineDispatcherProvider,
    private val registerMapper: RegisterMapper,
    private val loginMapper: LoginMapper
): AuthRepository {

    override suspend fun register(params: RegisterParams): Result<Register> {
        val apiResult = remote.register(dispatcher.io, params)
        return when (apiResult) {
            is Result.Success -> Result.Success(registerMapper.map(apiResult.data))
            is Result.Error -> Result.Error(apiResult.cause, apiResult.code, apiResult.errorMessage)
            else -> Result.Error()
        }
    }

    override suspend fun login(params: LoginParams): Result<Login> {
        val apiResult = remote.login(dispatcher.io, params)
        return when (apiResult) {
            is Result.Success -> Result.Success(loginMapper.map(apiResult.data))
            is Result.Error -> Result.Error(apiResult.cause, apiResult.code, apiResult.errorMessage)
            else -> Result.Error()
        }
    }

}