package com.abdhilabs.rodavis.data.source.remote.service

import com.abdhilabs.rodavis.data.source.remote.response.AuthResponse
import com.abdhilabs.rodavis.domain.entity.params.LoginParams
import com.abdhilabs.rodavis.domain.entity.params.RegisterParams
import retrofit2.http.Body
import retrofit2.http.POST

interface AuthService {

    @POST("/api/users/register")
    suspend fun register(@Body params: RegisterParams): AuthResponse

    @POST("/api/users/login")
    suspend fun login(@Body params: LoginParams): AuthResponse
}