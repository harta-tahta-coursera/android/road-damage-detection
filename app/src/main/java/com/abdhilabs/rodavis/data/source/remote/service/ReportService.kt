package com.abdhilabs.rodavis.data.source.remote.service

import com.abdhilabs.rodavis.data.source.remote.response.AddReportResponse
import com.abdhilabs.rodavis.data.source.remote.response.ReportResponse
import okhttp3.MultipartBody
import okhttp3.RequestBody
import retrofit2.http.*

interface ReportService {

    @JvmSuppressWildcards
    @Multipart
    @POST("/api/reports")
    suspend fun report(
        @Part photo: MultipartBody.Part,
        @PartMap map: Map<String, RequestBody>
    ): AddReportResponse

    @GET("/api/reports")
    suspend fun reports(): ReportResponse

    @GET("/api/reports/history")
    suspend fun reportHistory(): ReportResponse
}