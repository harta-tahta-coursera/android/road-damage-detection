package com.abdhilabs.rodavis

import android.app.Application
import dagger.android.AndroidInjector
import dagger.android.DispatchingAndroidInjector
import dagger.android.HasAndroidInjector
import com.abdhilabs.rodavis.di.AppComponent
import com.abdhilabs.rodavis.di.DaggerAppComponent
import com.abdhilabs.rodavis.di.module.ContextModule
import com.abdhilabs.rodavis.di.module.EncryptPreferenceModule
import com.abdhilabs.rodavis.di.module.NetworkModule
import timber.log.Timber
import javax.inject.Inject

class BaseApp: Application(), HasAndroidInjector {

    @Inject
    lateinit var androidInjector: DispatchingAndroidInjector<Any>

    private lateinit var appComponent: AppComponent

    override fun androidInjector(): AndroidInjector<Any> = androidInjector

    override fun onCreate() {
        super.onCreate()
        initAppDI()
        initTimber()
    }

    private fun initAppDI() {
        appComponent = DaggerAppComponent
            .builder()
            .contextModule(ContextModule(this))
            .networkModule(NetworkModule())
            .encryptPreferenceModule(EncryptPreferenceModule())
            .build()
        appComponent.inject(this)
    }

    private fun initTimber() {
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }

    open fun getComponent(): AppComponent {
        return appComponent
    }
}