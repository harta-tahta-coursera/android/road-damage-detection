package com.abdhilabs.rodavis.domain.usecase

import com.abdhilabs.coreandroid.abstraction.UseCase
import com.abdhilabs.rodavis.data.vo.Result
import com.abdhilabs.rodavis.domain.entity.model.Register
import com.abdhilabs.rodavis.domain.entity.params.RegisterParams
import com.abdhilabs.rodavis.domain.repository.AuthRepository
import javax.inject.Inject

class PostRegisterUseCase @Inject constructor(private val repository: AuthRepository) :
    UseCase<RegisterParams, Result<Register>>() {
    override suspend fun invoke(params: RegisterParams): Result<Register> {
        return repository.register(params)
    }
}