package com.abdhilabs.rodavis.domain.usecase

import com.abdhilabs.coreandroid.abstraction.UseCase
import com.abdhilabs.rodavis.data.vo.Result
import com.abdhilabs.rodavis.domain.entity.model.History
import com.abdhilabs.rodavis.domain.repository.ReportRepository
import javax.inject.Inject

class GetReportHistoryUseCase @Inject constructor(private val repository: ReportRepository) :
    UseCase<UseCase.None, Result<List<History>>>() {
    override suspend fun invoke(params: None): Result<List<History>> {
        return repository.getReportHistory()
    }
}