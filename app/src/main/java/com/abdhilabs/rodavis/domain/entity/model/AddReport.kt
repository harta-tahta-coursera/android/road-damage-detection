package com.abdhilabs.rodavis.domain.entity.model

import androidx.annotation.Keep

@Keep
data class AddReport(
    val address: String,
    val classes: List<String>,
    val dateReported: String,
    val id: Int,
    val imageUrl: String,
    val location: Location,
    val note: String,
    val reporterName: String,
    val status: String
) {
    data class Location(
        val lat: Double,
        val lng: Double
    )
}