package com.abdhilabs.rodavis.domain.entity

import com.abdhilabs.rodavis.abstraction.BaseItemModel

interface ItemClickListener {
    fun onClick(item: BaseItemModel)
}