package com.abdhilabs.rodavis.domain.entity.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class LocationModel(
    var locationAddress: String = "",
    var locationAdmin: String = "",
    var locationSubAdmin: String = "",
    var locationLocality: String = "",
    var locationSubLocality: String = "",
    var locationLatitude: String = "",
    var locationLongitude: String = ""
) : Parcelable