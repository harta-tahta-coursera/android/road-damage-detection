package com.abdhilabs.rodavis.domain.entity.model

import android.os.Parcelable
import androidx.annotation.Keep
import com.abdhilabs.rodavis.abstraction.BaseItemModel
import com.abdhilabs.rodavis.data.factory.ItemTypeFactory
import kotlinx.parcelize.Parcelize

@Keep
@Parcelize
data class Report(
    val dateReported: String,
    val id: Int,
    val imageUrl: String,
    val address: String,
    val location: Location,
    val note: String,
    val reporterName: String,
    val status: String,
    val classes: List<String>
) : BaseItemModel(), Parcelable {

    @Parcelize
    data class Location(
        val lat: Double,
        val lng: Double
    ) : Parcelable

    override fun type(itemTypeFactory: ItemTypeFactory): Int {
        return itemTypeFactory.type(this)
    }
}