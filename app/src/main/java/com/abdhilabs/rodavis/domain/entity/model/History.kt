package com.abdhilabs.rodavis.domain.entity.model

import androidx.annotation.Keep
import com.abdhilabs.rodavis.abstraction.BaseItemModel
import com.abdhilabs.rodavis.data.factory.ItemTypeFactory

@Keep
data class History(
    val dateReported: String,
    val id: Int,
    val imageUrl: String,
    val address: String,
    val location: Location,
    val note: String,
    val reporterName: String,
    val status: String,
    val classes: List<String>
) : BaseItemModel() {

    data class Location(
        val lat: Double,
        val lng: Double
    )

    override fun type(itemTypeFactory: ItemTypeFactory): Int {
        return itemTypeFactory.type(this)
    }
}

fun History.toReport(): Report {
    return Report(
        dateReported = dateReported,
        id = id,
        imageUrl = imageUrl,
        address = address,
        location = Report.Location(this.location.lat, this.location.lng),
        note = note,
        reporterName = reporterName,
        status = status,
        classes = classes
    )
}