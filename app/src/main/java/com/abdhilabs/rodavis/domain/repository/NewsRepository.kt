package com.abdhilabs.rodavis.domain.repository

import com.abdhilabs.rodavis.data.vo.Result
import com.abdhilabs.rodavis.domain.entity.model.Report

interface NewsRepository {
    suspend fun getNews(): Result<List<Report>>
}