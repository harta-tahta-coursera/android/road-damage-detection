package com.abdhilabs.rodavis.domain.entity.params

import androidx.annotation.Keep

@Keep
data class LoginParams(
    val email: String,
    val password: String
)
