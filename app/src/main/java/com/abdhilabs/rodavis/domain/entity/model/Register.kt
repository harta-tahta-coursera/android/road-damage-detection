package com.abdhilabs.rodavis.domain.entity.model

import androidx.annotation.Keep

@Keep
data class Register(
    val message: String,
    val status: Int
)
