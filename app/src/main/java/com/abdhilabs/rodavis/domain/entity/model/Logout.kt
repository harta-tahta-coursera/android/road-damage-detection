package com.abdhilabs.rodavis.domain.entity.model

import androidx.annotation.Keep

@Keep
data class Logout(
    val message: String,
    val status: Boolean,
    val userId: String
)
