package com.abdhilabs.rodavis.domain.entity.params

import androidx.annotation.Keep

@Keep
data class ReportParams(
    val lat: Float,
    val long: Float,
    val img: String,
    val note: String
)
