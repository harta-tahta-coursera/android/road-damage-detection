package com.abdhilabs.rodavis.domain.entity.model

import androidx.annotation.Keep

@Keep
data class Error(
    val message: String,
    val status: String
)
