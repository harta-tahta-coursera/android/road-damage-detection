package com.abdhilabs.rodavis.domain.entity

import com.abdhilabs.rodavis.abstraction.BaseItemModel
import com.abdhilabs.rodavis.data.factory.ItemTypeFactory

object Shimmer {

    val shimmerNews = listOf(
        ShimmerNews(""),
        ShimmerNews(""),
        ShimmerNews("")
    )
}

data class ShimmerNews(
    val title: String? = ""
) : BaseItemModel() {
    override fun type(itemTypeFactory: ItemTypeFactory): Int {
        return itemTypeFactory.type(this)
    }
}