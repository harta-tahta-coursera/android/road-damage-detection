package com.abdhilabs.rodavis.domain.entity.model

import android.graphics.RectF

data class DetectionResult(val boundingBox: RectF, val text: String)

