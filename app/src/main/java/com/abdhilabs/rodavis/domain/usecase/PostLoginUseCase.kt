package com.abdhilabs.rodavis.domain.usecase

import com.abdhilabs.coreandroid.abstraction.UseCase
import com.abdhilabs.rodavis.data.vo.Result
import com.abdhilabs.rodavis.domain.entity.model.Login
import com.abdhilabs.rodavis.domain.entity.params.LoginParams
import com.abdhilabs.rodavis.domain.repository.AuthRepository
import javax.inject.Inject

class PostLoginUseCase @Inject constructor(private val repository: AuthRepository) :
    UseCase<LoginParams, Result<Login>>() {
    override suspend fun invoke(params: LoginParams): Result<Login> {
        return repository.login(params)
    }
}