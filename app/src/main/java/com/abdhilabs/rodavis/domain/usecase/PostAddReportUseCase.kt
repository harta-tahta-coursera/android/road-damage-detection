package com.abdhilabs.rodavis.domain.usecase

import com.abdhilabs.coreandroid.abstraction.UseCase
import com.abdhilabs.rodavis.data.vo.Result
import com.abdhilabs.rodavis.domain.entity.model.AddReport
import com.abdhilabs.rodavis.domain.repository.ReportRepository
import okhttp3.MultipartBody
import okhttp3.RequestBody
import javax.inject.Inject

class PostAddReportUseCase @Inject constructor(private val repository: ReportRepository) :
    UseCase<PostAddReportUseCase.Params, Result<AddReport>>() {
    override suspend fun invoke(params: Params): Result<AddReport> {
        return repository.addReport(params.photo, params.map)
    }

    data class Params(
        val photo: MultipartBody.Part,
        val map: HashMap<String, RequestBody>
    )
}