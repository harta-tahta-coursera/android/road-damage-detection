package com.abdhilabs.rodavis.domain.repository

import com.abdhilabs.rodavis.data.vo.Result
import com.abdhilabs.rodavis.domain.entity.model.AddReport
import com.abdhilabs.rodavis.domain.entity.model.History
import okhttp3.MultipartBody
import okhttp3.RequestBody
import java.util.HashMap

interface ReportRepository {
    suspend fun addReport(
        photo: MultipartBody.Part,
        map: HashMap<String, RequestBody>
    ): Result<AddReport>
    suspend fun getReportHistory(): Result<List<History>>
}