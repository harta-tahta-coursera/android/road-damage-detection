package com.abdhilabs.rodavis.domain.repository

import com.abdhilabs.rodavis.data.vo.Result
import com.abdhilabs.rodavis.domain.entity.model.Login
import com.abdhilabs.rodavis.domain.entity.model.Register
import com.abdhilabs.rodavis.domain.entity.params.LoginParams
import com.abdhilabs.rodavis.domain.entity.params.RegisterParams

interface AuthRepository {
    suspend fun register(params: RegisterParams): Result<Register>
    suspend fun login(params: LoginParams): Result<Login>
}