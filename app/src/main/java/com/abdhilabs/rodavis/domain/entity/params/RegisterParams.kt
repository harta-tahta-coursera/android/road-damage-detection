package com.abdhilabs.rodavis.domain.entity.params

import androidx.annotation.Keep

@Keep
data class RegisterParams(
    val name: String,
    val phoneNumber: String,
    val email: String,
    val password: String
)
