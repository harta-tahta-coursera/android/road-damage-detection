package com.abdhilabs.rodavis.domain.usecase

import com.abdhilabs.coreandroid.abstraction.UseCase
import com.abdhilabs.rodavis.data.vo.Result
import com.abdhilabs.rodavis.domain.entity.model.Report
import com.abdhilabs.rodavis.domain.repository.NewsRepository
import javax.inject.Inject

class GetNewsUseCase @Inject constructor(private val repository: NewsRepository) :
    UseCase<UseCase.None, Result<List<Report>>>() {
    override suspend fun invoke(params: None): Result<List<Report>> {
        return repository.getNews()
    }
}