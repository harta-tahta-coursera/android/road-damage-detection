package com.abdhilabs.rodavis.domain.entity.model

import androidx.annotation.Keep

@Keep
data class Login(
    val token: String,
    val email: String,
    val name: String,
    val phoneNumber: String,
)
