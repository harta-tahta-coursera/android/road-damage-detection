package com.abdhilabs.rodavis.abstraction

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.abdhilabs.rodavis.data.factory.ItemTypeFactory
import com.abdhilabs.rodavis.domain.entity.ItemClickListener

open class BaseRecyclerViewAdapter(
    private val itemTypeFactory: ItemTypeFactory,
    private val itemClickListener: ItemClickListener?,
    private val items: ArrayList<BaseItemModel> = arrayListOf()
) : RecyclerView.Adapter<BaseViewHolder<BaseItemModel>>() {

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BaseViewHolder<BaseItemModel> {
        val view = inflaterView(viewType, parent)
        return itemTypeFactory.createViewHolder(
            view,
            parent,
            viewType
        ) as BaseViewHolder<BaseItemModel>
    }

    private fun inflaterView(viewType: Int, viewGroup: ViewGroup): View {
        return LayoutInflater.from(viewGroup.context).inflate(viewType, viewGroup, false)
    }

    override fun getItemCount(): Int = items.size

    override fun onBindViewHolder(holder: BaseViewHolder<BaseItemModel>, position: Int) {
        holder.bind(items[position], itemClickListener)
    }

    override fun getItemViewType(position: Int): Int {
        return items[position].type(itemTypeFactory)
    }

    fun refreshItems(items: List<BaseItemModel>) {
        this.items.clear()
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    fun addNewItems(items: List<BaseItemModel>) {
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    fun getItem(position: Int): BaseItemModel {
        return items[position]
    }
}