package com.abdhilabs.rodavis.abstraction

import com.abdhilabs.rodavis.data.factory.ItemTypeFactory

abstract class BaseItemModel {
    abstract fun type(itemTypeFactory: ItemTypeFactory): Int
}