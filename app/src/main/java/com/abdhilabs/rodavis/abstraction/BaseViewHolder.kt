package com.abdhilabs.rodavis.abstraction

import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding
import com.abdhilabs.rodavis.domain.entity.ItemClickListener

abstract class BaseViewHolder<T>(binding: ViewBinding) : RecyclerView.ViewHolder(binding.root) {
    abstract fun bind(item: T, clickListener: ItemClickListener?)
}