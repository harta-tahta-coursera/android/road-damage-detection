package com.abdhilabs.rodavis.di

import com.abdhilabs.rodavis.BaseApp
import com.abdhilabs.rodavis.di.module.*
import com.abdhilabs.rodavis.di.module.viewmodel.ViewModelModule
import dagger.Component
import dagger.android.AndroidInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        ActivityInjectionModule::class,
        FragmentInjectionModule::class,
        ContextModule::class,
        CoroutineDispatcherModule::class,
        ViewModelModule::class,
        RepositoryModule::class,
        NetworkModule::class,
        ApiModule::class,
        EncryptPreferenceModule::class,
    ]
)
interface AppComponent {
    fun inject(instance: BaseApp)
}