package com.abdhilabs.rodavis.di.module

import com.abdhilabs.rodavis.data.repositories.AuthRepositoryImpl
import com.abdhilabs.rodavis.data.repositories.NewsRepositoryImpl
import com.abdhilabs.rodavis.data.repositories.ReportRepositoryImpl
import com.abdhilabs.rodavis.domain.repository.AuthRepository
import com.abdhilabs.rodavis.domain.repository.NewsRepository
import com.abdhilabs.rodavis.domain.repository.ReportRepository
import dagger.Binds
import dagger.Module

@Module
interface RepositoryModule {

    @Binds
    fun bindAuthRepository(repository: AuthRepositoryImpl): AuthRepository

    @Binds
    fun bindNewsRepository(repository: ReportRepositoryImpl): ReportRepository

    @Binds
    fun bindReportRepository(repository: NewsRepositoryImpl): NewsRepository

}