package com.abdhilabs.rodavis.di.module

import com.abdhilabs.rodavis.data.source.remote.service.AccountService
import com.abdhilabs.rodavis.data.source.remote.service.AuthService
import com.abdhilabs.rodavis.data.source.remote.service.ReportService
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
class ApiModule {

    @Provides
    @Singleton
    fun provideAuthService(retrofit: Retrofit): AuthService {
        return retrofit.create(AuthService::class.java)
    }

    @Provides
    @Singleton
    fun provideReportService(retrofit: Retrofit): ReportService {
        return retrofit.create(ReportService::class.java)
    }

    @Provides
    @Singleton
    fun provideAccountService(retrofit: Retrofit): AccountService {
        return retrofit.create(AccountService::class.java)
    }
}