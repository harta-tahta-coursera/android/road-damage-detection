package com.abdhilabs.rodavis.di.module

import com.abdhilabs.rodavis.presentation.auth.AuthActivity
import com.abdhilabs.rodavis.presentation.detail.DetailActivity
import com.abdhilabs.rodavis.presentation.main.MainActivity
import com.abdhilabs.rodavis.presentation.report.ReportActivity
import com.abdhilabs.rodavis.presentation.report.location.PickLocationActivity
import com.abdhilabs.rodavis.presentation.report.picture.TakePictureActivity
import com.abdhilabs.rodavis.presentation.splash.SplashActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityInjectionModule {

    @ContributesAndroidInjector
    abstract fun contributesSplashActivity(): SplashActivity

    @ContributesAndroidInjector
    abstract fun contributesAuthActivity(): AuthActivity

    @ContributesAndroidInjector
    abstract fun contributesMainActivity(): MainActivity

    @ContributesAndroidInjector
    abstract fun contributesTakePictureActivity(): TakePictureActivity

    @ContributesAndroidInjector
    abstract fun contributesReportActivity(): ReportActivity

    @ContributesAndroidInjector
    abstract fun contributesPickLocationActivity(): PickLocationActivity

    @ContributesAndroidInjector
    abstract fun contributesDetailActivity(): DetailActivity
}