package com.abdhilabs.rodavis.di.module

import com.abdhilabs.rodavis.presentation.account.AccountFragment
import com.abdhilabs.rodavis.presentation.auth.LoginFragment
import com.abdhilabs.rodavis.presentation.auth.MainFragment
import com.abdhilabs.rodavis.presentation.auth.RegisterFragment
import com.abdhilabs.rodavis.presentation.news.NewsFragment
import com.abdhilabs.rodavis.presentation.report.ReportFragment
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentInjectionModule {

    @ContributesAndroidInjector
    abstract fun contributesMainFragment(): MainFragment

    @ContributesAndroidInjector
    abstract fun contributesRegisterFragment(): RegisterFragment

    @ContributesAndroidInjector
    abstract fun contributesLoginFragment(): LoginFragment

    @ContributesAndroidInjector
    abstract fun contributesReportFragment(): ReportFragment

    @ContributesAndroidInjector
    abstract fun contributesNewsFragment(): NewsFragment

    @ContributesAndroidInjector
    abstract fun contributesAccountFragment(): AccountFragment
}