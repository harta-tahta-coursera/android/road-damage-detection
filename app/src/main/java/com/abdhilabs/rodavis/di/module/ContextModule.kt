package com.abdhilabs.rodavis.di.module

import android.app.Application
import android.content.Context
import com.abdhilabs.rodavis.BaseApp
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ContextModule(val app: BaseApp) {

    @Provides
    @Singleton
    fun provideContext(): Context {
        return app
    }

    @Provides
    @Singleton
    fun provideApplication(): Application {
        return app
    }
}