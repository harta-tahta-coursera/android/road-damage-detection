package com.abdhilabs.rodavis.di.module

import dagger.Binds
import dagger.Module
import com.abdhilabs.rodavis.data.dispatcher.CoroutineDispatcherProvider
import com.abdhilabs.rodavis.data.dispatcher.DispatcherProvider

@Module
interface CoroutineDispatcherModule {

    @Binds
    fun bindDispatcher(dispatcherProvider: CoroutineDispatcherProvider): DispatcherProvider
}